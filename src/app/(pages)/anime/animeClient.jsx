"use client";

import CardAnime from "@/app/components/CardAnime";
import PopoverAnime from "@/app/components/PopoverAnime";
import { Button, ScrollShadow } from "@nextui-org/react";

export default function AnimeClient({ AllAnime }) {
  return (
    <ScrollShadow className="w-auto  h-screen" hideScrollBar>
      <div className="mb-20">
        <h1 className="text-center text-2xl py-6">Anime</h1>
        <div className="flex md:justify-center mt-6 ">
          <div className="grid grid-cols-2 gap-2 md:grid-cols-4  ">
            {AllAnime.map((m, i) => {
              return (
                <CardAnime
                  key={i}
                  title={m.title}
                  director={m.director}
                  year={m.year}
                  id={m.id}
                />
              );
            })}
          </div>
        </div>

        <div className="fixed bottom-4 left-3">
          <PopoverAnime contentName={"addAnime"}>
            <Button color="primary" radius="sm" variant="bordered">
              Add Anime
            </Button>
          </PopoverAnime>
        </div>
      </div>
    </ScrollShadow>
  );
}
