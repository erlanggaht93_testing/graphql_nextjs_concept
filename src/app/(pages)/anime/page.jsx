import { mutationsRequest, queryRequest } from '@/app/services/API/graphQLRequest'
import AnimeClient from './animeClient';
const queryGetAnime =
    `
 query {
    AllAnime {
      id
      title
      director
      year
    }
}
`;


export default async function page() {
    const { data: { AllAnime } } = await queryRequest(queryGetAnime, 'no-store')
    
    return <main>
      <AnimeClient AllAnime={AllAnime} />
    </main>
}
