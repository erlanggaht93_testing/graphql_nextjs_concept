import { ApolloClient, HttpLink, InMemoryCache, gql } from "@apollo/client";
import { registerApolloClient } from "@apollo/experimental-nextjs-app-support/rsc";
import ApolloClientPage from "./apolloClient";
import { data } from "autoprefixer";

export const { getClient } = registerApolloClient(() => {
  return new ApolloClient({
    cache: new InMemoryCache(),
    link: new HttpLink({
      uri: "http://localhost:4000/graphql",
    }),
    connectToDevTools: true,
    ssrMode:true
  });
});

const GET_ANIMES = gql`
  {
    AllAnime {
      id
      title
      director
      year
    }
  }
`;

export default async function page() {
  const { data : {AllAnime}, networkStatus } = await getClient().query({
    query: GET_ANIMES,
    context : {
      fetchOptions: {
        cache : "no-store"
      }
    }
  });
  return (
    <main>
      <ApolloClientPage AllAnime={AllAnime} />
    </main>
  )
}
