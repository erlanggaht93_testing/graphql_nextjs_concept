"use client";
import CardAnime from "@/app/components/CardAnime";
import PopoverAnime from "@/app/components/PopoverAnime";
import { Button, ScrollShadow } from "@nextui-org/react";

export default function ApolloClientPage({ AllAnime }) {
  return (
    <ScrollShadow className="w-auto h-screen" hideScrollBar>
    <div className="mb-20">
      <h1 className="text-center p-3 text-xl">Apollo</h1>
      <div className="flex md:justify-center mt-6 ">
      <div className="grid grid-cols-2 gap-2 md:grid-cols-4">
        {AllAnime.map((m, i) => {
          const { id, title, director, year } = m;
          return (
            <CardAnime
              key={i}
              id={id}
              title={title}
              director={director}
              year={year}
            />
          );
        })}
        </div>
      </div>

      <div className="fixed bottom-4 left-3">
        <PopoverAnime contentName={"addAnime"}>
          <Button color="primary" radius="sm" variant="bordered">
            Add Anime
          </Button>
        </PopoverAnime>
      </div>
    </div>
    </ScrollShadow>

  );
}
