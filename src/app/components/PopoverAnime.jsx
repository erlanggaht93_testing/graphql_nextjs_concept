import {
  Button,
  Input,
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@nextui-org/react";
import React, { useRef, useState, useTransition } from "react";
import { mutationsRequest } from "../services/API/graphQLRequest";
import { useRouter } from "next/navigation";

export default function PopoverAnime({ children, id, contentName }) {
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isPending, startTransition] = useTransition();
  const [isFetching, setIsFetching] = useState(false);

  const isMutating = isFetching || isPending;
  // mutation delete
  const deleteAnime = async () => {
    setIsFetching(true);
    const response = await mutationsRequest("deleteAnime", id, "no-store");
    setIsFetching(false);
    startTransition(() => {
      // tidak wajib
      setIsLoading(true);
      setTimeout(() => {
        // wajib agar tidak ke catch saat refatch
        setIsLoading(false)
        router.refresh();
        setIsOpen(false)
      }, 1200);
    });
  };

  return (
    <Popover
      showArrow
      offset={10}
      placement="center"
      backdrop={"blur"}
      isOpen={isOpen}
      onOpenChange={(open) => setIsOpen(open)}
      style={{ opacity: !isMutating ? 1 : 0.7 }}
    >
      <PopoverTrigger>{children}</PopoverTrigger>

      <PopoverContent className="w-[240px]">
        <div className="px-1 py-2 w-full">
          {contentName === "deleteAnime" && (
            <ContentDeleteAnime
              setIsOpen={setIsOpen}
              deleteAnime={deleteAnime}
              isLoading={isLoading}
            />
          )}
          {contentName === "addAnime" && (
            <ContentAddAnime
              setIsOpen={setIsOpen}
              deleteAnime={deleteAnime}
              isLoading={isLoading}
            />
          )}
        </div>
      </PopoverContent>
    </Popover>
  );
}

export const ContentDeleteAnime = ({ setIsOpen, deleteAnime, isLoading }) => {
  return (
    <>
      <p className="text-small font-bold text-foreground text-center">
        sure deleted ?
      </p>
      <div className="mt-2 flex flex-col gap-2 w-full">
        <Button isLoading={isLoading} onPress={() => deleteAnime()}>
          {isLoading ? "Wait ... " : "Yes"}
        </Button>
        <Button onPress={() => setIsOpen(false)}>No</Button>
      </div>
    </>
  );
};

export const ContentAddAnime = ({setIsOpen}) => {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [isPending, startTransition] = useTransition();
  const [isFetching, setFetching] = useState(false);

  const isMutating = isFetching || isPending;

  const [input, setInput] = useState({
    title: "",
    director: "",
    year: "",
  });

  const handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;

    setInput({
      ...input,
      [name]: value,
    });
  };

  const addAnime = async (e) => {
    e.preventDefault();

    setFetching(true);
    const response = await mutationsRequest(
      "addAnime",
      "",
      "no-store",
      input.title,
      input.director,
      parseInt(input.year)
    );
    setFetching(false);
    startTransition(() => {
      setIsLoading(true)
      setTimeout(() => {
        setIsLoading(false)
        router.refresh()
        setIsOpen(false)
      },1200)
    });
  };

  return (
    <>
      <div className="px-1 py-2 w-full">
        <p className="text-small font-bold text-foreground">Add Anime</p>
        <form
          onSubmit={(e) => addAnime(e)}
          className="mt-2 flex flex-col gap-2 w-full"
        >
          <Input
            // defaultValue="100%"
            label="Title"
            size="sm"
            variant="bordered"
            onChange={(e) => handleChange(e)}
            name="title"
          />
          <Input
            // defaultValue="300px"
            label="Director"
            size="sm"
            variant="bordered"
            onChange={(e) => handleChange(e)}
            name="director"
          />
          <Input
            // defaultValue="24px"
            label="Year"
            size="sm"
            variant="bordered"
            onChange={(e) => handleChange(e)}
            name="year"
          />
          <Button type="submit" color="primary" radius="sm" isLoading={isLoading}>
            Submit
          </Button>
        </form>
      </div>
    </>
  );
};
