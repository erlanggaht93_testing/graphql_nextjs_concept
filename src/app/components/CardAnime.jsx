import { Button, Card, CardFooter, CardHeader, Image } from "@nextui-org/react";
import PopoverAnime from "./PopoverAnime";
export default function CardAnime({ id, title, director, year }) {

  return (
    <>
      <Card
        isFooterBlurred
        className="bg-[#222] relative md:w-[290px] rounded-md text-white"
        shadow="md"
      >
        <CardHeader className="p-4 pb-0 flex-col items-start">
          <p className="text-md uppercase font-bold">{title}</p>
          <small className="text-default-500">{year}</small>
          <h4 className="font-bold text-xl">{director}</h4>
        </CardHeader>
        <Image
          alt="Card background"
          className="object-cover rounded-xl pt-6"
          src="https://nextui.org/images/hero-card.jpeg"
          width={370}
        />

        <CardFooter className="justify-between before:bg-white/10 border-white/20 border-1 overflow-hidden py-1 absolute before:rounded-xl rounded-large bottom-1 w-[calc(100%_-_8px)] shadow-small ml-1 z-10">
          <p className="text-tiny text-white/80">Available soon.</p>
          <PopoverAnime id={id} contentName={"deleteAnime"}>
          <Button
            className="text-tiny text-white bg-black/20"
            variant="flat"
            color="default"
            radius="lg"
            size="sm"
          >
            Delete
          </Button>
          </PopoverAnime>
        </CardFooter>
      </Card>
    </>
  );
}
