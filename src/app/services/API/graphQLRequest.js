
// Query
const queryRequest = async (query,cache) => {
    try {
        const response = await fetch('http://localhost:4000/graphql', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                query
            }),
            cache: cache
        })

        return await response.json()
    } catch (error) {
        throw new Error("CATCH:" + error)
    }
}

// Mutations 
/*
- findAnime
- addAnime
- updateAnime
- deleteAnime
*/

let mutations = { 
    findAnime: "findAnime", 
    addAnime: "addAnime", 
    updateAnime: "updateAnime", 
    deleteAnime: "deleteAnime" 
}
const mutationsRequest = async (nameMutation, id, cache = 'no-cache', ...addAnime) => {
    const [title, director, year] = addAnime
    let query;
    if (nameMutation === mutations.findAnime) {
        query = `
            mutation{
            findAnime(id:${id}) {
                id  
                title
                director
                year
            }
        }
        `
    }
    else if (nameMutation === mutations.addAnime) {
        query = `
            mutation {
              addAnime(title:"${title}",director:"${director}",year:${year}) {
                id                
                title
                director
                year
            }   
        }
        `
    } 
    else if (nameMutation === mutations.deleteAnime) {
        query = `
        mutation {
            deleteAnime(id: ${id}) {
                id,
                title,
                director,
                year
            }
        }
        `
    }
    else return

    try {
        const response = await fetch('http://localhost:4000/graphql', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                query
            }),
            cache: cache 
        }) 
        return response.json()
    } catch (error) {
        throw new Error("CATCH:" + error)
    }
}


export { queryRequest, mutationsRequest }