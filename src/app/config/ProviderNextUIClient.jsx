'use client'

import {NextUIProvider} from '@nextui-org/react'

export function ProviderNextUIClient({children}) {
  return (
    <NextUIProvider>
      {children}
    </NextUIProvider>
  )
}