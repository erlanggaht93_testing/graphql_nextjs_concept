const express = require("express");
const {graphqlHTTP} = require("express-graphql")
var cors = require('cors');

const app = express();
const schema = require('./schema/schema.js');
app.use(cors(
   {origin: "http://localhost:3000"}
));
 app.use('/graphql', graphqlHTTP
 ({
    schema,
    graphiql: true,
  }));

 app.listen(4000,() => {
    console.log('server is running on port' + 4000 )
 })
