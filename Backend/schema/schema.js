const axios = require('axios');
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLSchema,
    GraphQLNonNull
} = require('graphql');

const server = 'http://localhost:3002'

const animeType = new GraphQLObjectType({
    name: 'Anime',
    fields: () => ({
        id: { type: GraphQLInt },
        title: { type: GraphQLString },
        director: { type: GraphQLString },
        year: { type: GraphQLInt },
        image: {type: GraphQLString}
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        AllAnime: {
            type: new GraphQLList(animeType),
            resolve(parentValue, args) {
                return axios.get(`${server}/animes/`)
                    .then(res => {
                        return res.data
                    });
            }
        }
    }
});

const mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addAnime: {
            type: animeType,
            args: {
                title: { type: new GraphQLNonNull(GraphQLString) },
                director: { type: new GraphQLNonNull(GraphQLString) },
                year: { type: new GraphQLNonNull(GraphQLInt) },
            },
            resolve(parentValue, args) {
                return axios.post(`${server}/animes`, args).then(res => res.data);
            }
        },
        findAnime: {
            type: animeType,
            args: {
                id: { type: GraphQLInt },
            },
            resolve(parentValue, args) {
                return axios.get(`${server}/animes/` + args.id)
                    .then(res => res.data);
            }
        },
        updateAnime: {
            type: animeType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLInt) },
                title: { type: GraphQLString },
                director: { type: GraphQLString },
                year: { type: GraphQLInt },
                status: { type: GraphQLInt }
            },
            resolve(parentValue, args) {
                console.log(args)
                return axios.put(`${server}/animes/` + args.id, args)
                    .then(res => res.data);
            }
        },
        deleteAnime: {
            type: animeType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLInt)},
            },
            resolve(parentValue, args) {
                return axios.delete(`${server}/animes/` + args.id, args)
                    .then(res => res.data);
            }
        },
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation,
});